import os
import sys
from math import ceil

root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if root_dir not in sys.path:
    sys.path.append(root_dir)

from math import ceil

import numpy as np
from utils import lcm


class Base2DEnv:
    def __init__(self, args):
        self.box_ids = args["box_ids"]
        # self.box_locations = args.box_locations

        self.box_width = args["box_width"]
        self.box_length = args["box_length"]
        self.box_height = args["box_height"]

        self.floor = args["floor"]
        self.pallet_width = args["pallet_width"]
        self.pallet_length = args["pallet_length"]
        self.pallet_height = int(self.floor * max(self.box_height))

        self.grid_size = args["grid_size"]
        self.orientation = args["orientation"]
        self.origin_point = [
            args["origin_point"][0] - self.pallet_width / 2,
            args["origin_point"][1] - self.pallet_length / 2,
            args["origin_point"][2],
        ]

        self.put_boxes = list()

        self.ids_to_box_prop = {
            self.box_ids[i]: [
                self.box_width[i],
                self.box_length[i],
                self.box_height[i],
            ]
            for i in range(len(self.box_ids))
        }

        self.ids_to_box_grid = {
            self.box_ids[i]: [
                ceil(
                    self.grid_size / (self.pallet_width / self.box_width[i])
                ),  # width 칸 수
                ceil(
                    self.grid_size / (self.pallet_length / self.box_length[i])
                ),  # length 칸 수
                1,  # height 칸 수
            ]
            for i in range(len(self.box_ids))
        }

        self.action_dim = int(self.grid_size * self.grid_size * self.orientation)
        self.actions = list()
        for o in range(self.orientation):
            for w in range(self.grid_size):
                for l in range(self.grid_size):
                    self.actions.append([w, l, o])
        self.episode_length = len(self.box_ids)
        self.r_type = "simple" if args["r_type"] is None else args["r_type"]
        self.barcode_loc = args["barcode_loc"]
        self.coords_list = []

    def reset(self):
        self.put_boxes.clear()
        self._step = 0
        self.done = False
        self.current_box_id = self._step
        self.current_box_prop = self.ids_to_box_prop[self.current_box_id]
        self.current_box_grid = self.ids_to_box_grid[self.current_box_id]
        self.curr_layer = 0
        self.height_map = np.zeros((self.grid_size, self.grid_size), dtype=np.int32)

        state = self.height_map.copy()
        mask = self.get_mask(state, self.current_box_grid)

        state = np.expand_dims(state, axis=0)
        mask = np.expand_dims(mask, axis=0)

        return state, mask

    def step(self, action):
        """
            - - - - - - - - - (length)
           |
           |
           |
           |
        (width)
        """
        # 1. drop the box
        # 2. get next state
        # 3. get masking
        # 3. calculate reward
        # location = [x, y, o]

        action_idx = action[0]
        location = self.actions[action_idx]
        orientation = location[2]
        current_box_id = self._step
        current_box = self.ids_to_box_prop[self._step]
        current_grid_box = self.ids_to_box_grid[self._step]

        if orientation == 0:
            box_grid_width = current_grid_box[0]  # grid size
            box_grid_length = current_grid_box[1]
            box_grid_height = current_grid_box[2]

        elif orientation == 1:
            box_grid_length = current_grid_box[0]  # grid size
            box_grid_width = current_grid_box[1]
            box_grid_height = current_grid_box[2]

        # 0510 bug: max height로 넣어야 함(fixed, line 132, 133)
        r00 = location[0]
        r01 = location[0] + box_grid_width
        r10 = location[1]
        r11 = location[1] + box_grid_length

        max_h = np.max(self.height_map[r00:r01, r10:r11])
        max_h += box_grid_height
        self.height_map[r00:r01, r10:r11] = max_h
        self._step += 1

        self.put_boxes.append(current_box)

        next_state = self.height_map.copy()
        reward = self.get_reward(next_state, current_box)

        if int(self._step) >= len(self.box_ids):  # if all drop, done is True
            next_box = None
            self.done = True
        else:
            next_box = self.ids_to_box_grid[self._step]  # else, done is False

        mask = self.get_mask(next_state, next_box)

        # 1. simple masking
        # if not done and mask is 0, done is True(terminate)
        if np.sum(mask) == 0 and (int(self._step) < len(self.box_ids)):
            self.done = True

        next_state = np.expand_dims(next_state, axis=0)
        mask = np.expand_dims(mask, axis=0)
        action_to_unity = self.action_to_unity(action, current_box_id)

        return next_state, mask, reward, self.done, action_to_unity

    def get_mask(self, current_state, next_box=None):
        if next_box == None:
            # is done
            action_mask1 = np.zeros((self.grid_size, self.grid_size), dtype=np.int32)
            action_mask2 = np.zeros((self.grid_size, self.grid_size), dtype=np.int32)
            mask = np.hstack(
                (
                    action_mask1.reshape(
                        -1,
                    ),
                    action_mask2.reshape(
                        -1,
                    ),
                )
            )
            return mask
        else:
            action_mask1 = np.zeros((self.grid_size, self.grid_size), dtype=np.int32)
            action_mask2 = np.zeros((self.grid_size, self.grid_size), dtype=np.int32)
            # not done, calculate masking position
            # current_state is height_map
            box_width_grid = next_box[0]
            box_length_grid = next_box[1]
            box_height_grid = next_box[2]

            for i in range(self.grid_size - box_width_grid + 1):
                for j in range(self.grid_size - box_length_grid + 1):
                    if (
                        self.check_box(
                            current_state,
                            box_width_grid,
                            box_length_grid,
                            i,
                            j,
                            box_height_grid,
                        )
                        >= 0
                    ):
                        action_mask1[i][j] = 1

            for i in range(self.grid_size - box_length_grid + 1):
                for j in range(self.grid_size - box_width_grid + 1):
                    if (
                        self.check_box(
                            current_state,
                            box_length_grid,
                            box_width_grid,
                            i,
                            j,
                            box_height_grid,
                        )
                        >= 0
                    ):
                        action_mask2[i][j] = 1

            mask = np.hstack(
                (
                    action_mask1.reshape(
                        -1,
                    ),
                    action_mask2.reshape(
                        -1,
                    ),
                )
            )
            return mask

    def check_box(self, state, box_width_grid, box_length_grid, i, j, box_height_grid):
        """
        box_width_grid: grid size of box width
        box_length: grid size of box length
        i: location width
        j: location length
        box_height: grid size of box height

        """
        # 0. check width, length boundary
        if i + box_width_grid > self.grid_size or j + box_length_grid > self.grid_size:
            return -1
        if i < 0 or j < 0:
            return -1

        # 0-1. check layer
        # if state[i, j] >

        # 1. if drop the next box, get max height value & position
        rectangle = state[i : i + box_width_grid, j : j + box_length_grid]
        r00 = rectangle[0, 0]
        r01 = rectangle[box_width_grid - 1, 0]
        r10 = rectangle[0, box_length_grid - 1]
        r11 = rectangle[box_width_grid - 1, box_length_grid - 1]
        max_pos_h = max(r00, r01, r10, r11)

        # 2. check drop position is stable?, at least 2 position is same height <- powerful assumption
        check = (
            int(r00 == max_pos_h)
            + int(r01 == max_pos_h)
            + int(r10 == max_pos_h)
            + int(r11 == max_pos_h)
        )
        if check < 2:
            return -1

        # 3. check height boundary
        max_h = np.max(rectangle)
        assert max_h >= 0
        if max_h + box_height_grid > self.floor:
            return -1

        return max_h

    def get_reward(self, action, current_box):
        if self.r_type == "simple":
            total_volume = int(
                self.pallet_width * self.pallet_length * self.pallet_height
            )
            box_width, box_length, box_height = map(float, current_box)
            reward = float(box_width * box_length * box_height) / total_volume

        elif self.r_type == "accumulate":
            total_volume = int(
                self.pallet_width * self.pallet_length * self.pallet_height
            )
            box_width, box_length, box_height = map(float, current_box)
            box_volume = float(box_width * box_length * box_height)
            exist_box_volume = box_volume * float(self._step - 1)
            accumulate_box_volume = box_volume + exist_box_volume
            accumulate_reward = float(accumulate_box_volume) / total_volume
            reward = accumulate_reward

        elif self.r_type == "10simple":
            total_volume = int(
                self.pallet_width * self.pallet_length * self.pallet_height
            )
            box_width, box_length, box_height = map(float, current_box)

            reward = 10 * float(
                float(box_width * box_length * box_height) / total_volume
            )

        elif self.r_type == "10accumulate":
            total_volume = int(
                self.pallet_width * self.pallet_length * self.pallet_height
            )
            box_width, box_length, box_height = map(float, current_box)
            box_volume = float(box_width * box_length * box_height)
            exist_box_volume = box_volume * float(self._step - 1)
            accumulate_box_volume = box_volume + exist_box_volume
            accumulate_reward = 10 * float(accumulate_box_volume) / total_volume
            reward = accumulate_reward

        elif self.r_type == "2d10accumulate":
            total_area = int(self.pallet_width * self.pallet_length)
            box_width, box_length, box_height = map(float, current_box)
            box_area = float(box_width * box_length)
            exist_box_area = box_area * float(self._step - 1)
            accumulate_box_area = box_area + exist_box_area
            accumulate_reward = 10 * float(accumulate_box_area) / total_area
            reward = accumulate_reward

        elif self.r_type == "inamct":
            # box reward
            total_volume = int(
                self.pallet_width * self.pallet_length * self.pallet_height
            )
            box_width, box_length, box_height = map(float, current_box)
            box_volume = float(box_width * box_length * box_height)
            exist_box_volume = box_volume * float(self._step - 1)
            accumulate_box_volume = box_volume + exist_box_volume
            box_reward = 10 * float(accumulate_box_volume) / total_volume

            # 값 0.5 잘 고민
            x_grid = 2 * action[0]

            accumulate_reward = box_reward - x_grid
            reward = accumulate_reward

        return reward

    def action_to_unity(self, action, current_box_id):
        action_idx = action[0]
        location = self.actions[action_idx]
        # 1. drop position in grid map = action
        # 1-1. get height of drop position
        lx, ly = location[0], location[1]
        curr_box_height = self.put_boxes[-1][-1]

        orientation = location[2]
        if orientation == 0:
            rot = 90
        elif orientation == 1:
            rot = 0
        elif orientation == 2:
            rot = 270
        elif orientation == 3:
            rot = 180
        # rot = 90 if orientation == 0 else 0  # setting orientation, 내가 생각한것과 반대임

        box_id = current_box_id
        current_box = self.ids_to_box_prop[box_id]
        # 2. box information in unity(real)
        box_width, box_length, box_height = (
            current_box[0],
            current_box[1],
            current_box[2],
        )
        # 3. grid cell size(width, lenght, height) in grid map
        x_grid_size = float(self.pallet_width / self.grid_size)
        y_grid_size = float(self.pallet_length / self.grid_size)
        z_grid_size = box_height

        # 4. drop position in unity environment
        # position = origin_point + grid 개수 * grid의 크기
        _x = self.origin_point[0] + lx * x_grid_size
        _y = self.origin_point[1] + ly * y_grid_size
        _z = self.origin_point[2]

        # 5. transform drop position along orientation
        # unity 상에서는 box를 lfb(?)를 기준으로 하지 않고 box의 정중앙을 기준으로
        # 하기 때문에 position의 값을 박스의 꼭짓점 기준이 아닌 정가운데로 이동시켜야 함
        if orientation == 0 or orientation == 2:
            u_x = _x + box_width / 2
            u_y = _y + box_length / 2
            u_z = _z + z_grid_size / 2 + (curr_box_height - 1) * z_grid_size
        elif orientation == 1 or orientation == 3:
            u_x = _x + box_length / 2
            u_y = _y + box_width / 2
            u_z = _z + z_grid_size / 2 + (curr_box_height - 1) * z_grid_size

        u_x = int((u_x + 30) * 1000)
        u_y = int((u_y + 30) * 1000)
        u_z = int((u_z + 30) * 1000)

        action = [box_id, u_x, u_z, u_y, 0, rot, 0]

        self.coords_list.append(action)

        return action

    def close(self):
        pass
