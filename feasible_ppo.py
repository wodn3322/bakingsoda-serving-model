import os
import sys
import numpy as np
import torch
import torch.nn as nn
from torch.distributions import Categorical
from torch.nn.modules import loss
import torch.nn.functional as F
from tqdm import tqdm

root_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
if root_dir not in sys.path:
    sys.path.append(root_dir)

from utils import conv2d_output_shape, discount_cumsum


class Buffer:
    """
    A buffer for storing samples
    """

    def __init__(
        self,
        grid_size,
        buf_size,
        action_dim,
        gamma=0.99,
        gae_lambda=0.95,
        device=torch.device("cpu"),
    ):
        # grid_size: (1, grid_size, grid_size)
        self.grid_buf = np.zeros(
            [buf_size, grid_size[2], grid_size[1], grid_size[0]], dtype=np.int
        )
        self.act_buf = np.zeros(buf_size, dtype=np.float32)  # action
        self.adv_buf = np.zeros(buf_size, dtype=np.float32)  # advantage
        self.rew_buf = np.zeros(buf_size, dtype=np.float32)  # reward
        self.ret_buf = np.zeros(buf_size, dtype=np.float32)  # target value
        self.val_buf = np.zeros(buf_size, dtype=np.float32)  # value
        self.done_buf = np.zeros(buf_size, dtype=np.float32)  # done
        self.logp_buf = np.zeros(buf_size, dtype=np.float32)  # log probability
        self.mask_buf = np.zeros([buf_size, action_dim], dtype=np.float32)  # mask
        self.gamma, self.lam = gamma, gae_lambda
        self.ptr, self.path_start_idx, self.max_size = 0, 0, buf_size  # internal index

    def store(self, obs, act, mask, rew, val, logp, done):
        """
        Store a single transition (state, action, reward, value, log prob) to the buffer
        case 0. obs: grid information (shape: (1, grid_size, grid_size))
        case 1. obs: grid information (shape: (1, grid_size, grid_size))
        """

        assert self.ptr < self.max_size
        self.grid_buf[self.ptr] = obs
        self.act_buf[self.ptr] = act
        self.rew_buf[self.ptr] = rew
        self.val_buf[self.ptr] = val
        self.done_buf[self.ptr] = done
        self.logp_buf[self.ptr] = logp
        self.mask_buf[self.ptr] = mask
        self.ptr += 1

    def store_from_other_buffer(self, buffer=None):
        """
        Store the whole episode from another buffer
        """
        if buffer is None:
            return
        assert self.ptr <= self.max_size
        self.grid_buf[
            self.path_start_idx : self.path_start_idx + buffer.ptr
        ] = buffer.grid_buf[: buffer.ptr]
        self.act_buf[
            self.path_start_idx : self.path_start_idx + buffer.ptr
        ] = buffer.act_buf[: buffer.ptr]
        self.rew_buf[
            self.path_start_idx : self.path_start_idx + buffer.ptr
        ] = buffer.rew_buf[: buffer.ptr]
        self.val_buf[
            self.path_start_idx : self.path_start_idx + buffer.ptr
        ] = buffer.val_buf[: buffer.ptr]
        self.logp_buf[
            self.path_start_idx : self.path_start_idx + buffer.ptr
        ] = buffer.logp_buf[: buffer.ptr]
        self.done_buf[
            self.path_start_idx : self.path_start_idx + buffer.ptr
        ] = buffer.done_buf[: buffer.ptr]
        self.mask_buf[
            self.path_start_idx : self.path_start_idx + buffer.ptr
        ] = buffer.mask_buf[: buffer.ptr]

        self.adv_buf[
            self.path_start_idx : self.path_start_idx + buffer.ptr
        ] = buffer.adv_buf[: buffer.ptr]
        self.ret_buf[
            self.path_start_idx : self.path_start_idx + buffer.ptr
        ] = buffer.ret_buf[: buffer.ptr]

        # Update internal index
        self.path_start_idx += buffer.ptr
        self.ptr += buffer.ptr

    # This function calculate advantage and target value for each episode
    def finish_path(self, last_val=0, last_reward=None):
        if last_reward is not None:
            self.rew_buf[self.ptr - 1] = last_reward

        path_slice = slice(self.path_start_idx, self.ptr)
        rews = np.append(self.rew_buf[path_slice], last_val)
        vals = np.append(self.val_buf[path_slice], last_val)

        # # GAE-Lambda advantage calculation
        # td_target = rews[:-1] + self.gamma * vals[1:]
        # td_error = td_target - vals[:-1]
        # self.adv_buf[path_slice] = discount_cumsum(td_error, self.gamma * self.lam)
        # self.ret_buf[path_slice] = self.adv_buf[path_slice] + vals[:-1]

        # GAE-Lambda advantage calculation
        deltas = rews[:-1] + self.gamma * vals[1:] - vals[:-1]
        self.adv_buf[path_slice] = discount_cumsum(deltas, self.gamma * self.lam)
        # Computes rewards-to-go
        self.ret_buf[path_slice] = discount_cumsum(rews, self.gamma)[:-1]

        # update internal index
        self.path_start_idx = self.ptr

    def get(self, device):
        """
        get data from buffer and reset internal index
        """

        data = dict(
            obs=self.grid_buf[: self.ptr, :, :, :],
            act=self.act_buf[: self.ptr],
            mask=self.mask_buf[: self.ptr, :],
            ret=self.ret_buf[: self.ptr],
            adv=self.adv_buf[: self.ptr],
            done=self.done_buf[: self.ptr],
            logp=self.logp_buf[: self.ptr],
        )

        self.ptr, self.path_start_idx = 0, 0

        return {
            k: torch.as_tensor(v, dtype=torch.float32).to(device)
            for k, v in data.items()
        }

    def reset(self):
        self.ptr, self.path_start_idx = 0, 0


class PPO(nn.Module):
    def __init__(
        self,
        grid_size,
        action_dim,
        kernel_size,
        hidden_size=256,
        conv_activation=nn.ReLU,
        fc_activation=nn.ReLU,
        buf_size=None,
        learning_rate=1e-5,
        train_iters=100,
        clipping_ratio=0.2,
        discount=0.99,
        gae_lambda=0.95,
        clip_norm=1000,
        entropy_ratio=0.01,
        shared_net=0,
        mask_predict=1,
        value_loss_func=loss.MSELoss,
        mask_loss_func=F.mse_loss,
        optimizer_func=torch.optim.Adam,
        device=torch.device("cpu"),
    ):

        super().__init__()
        self.grid_size = grid_size
        self.action_dim = action_dim
        self.kernel_size = kernel_size
        self.hidden_size = hidden_size
        self.shared_net = shared_net
        self.mask_predict = mask_predict
        self.value_loss_func = value_loss_func
        self.mask_loss_func = mask_loss_func
        self.conv_activation_func = conv_activation
        self.fc_activation_func = fc_activation
        self.discount = discount
        self.gae_lambda = gae_lambda

        self.conv = nn.Sequential(
            nn.Conv2d(self.grid_size[-1], 32, self.kernel_size[0]),
            conv_activation(),
            nn.Conv2d(32, 64, self.kernel_size[1]),
            conv_activation(),
            nn.Conv2d(64, 64, self.kernel_size[2]),
            conv_activation(),
        ).to(device)

        h, w, c = self.grid_size
        self.embedding_size = self.conv_out_size(self.conv, h, w, c)

        if shared_net == 1:
            self.fc = nn.Sequential(
                nn.Linear(self.embedding_size, hidden_size),
                fc_activation(),
                nn.Linear(hidden_size, hidden_size),
                fc_activation(),
            )
            self.embedding_size = hidden_size

        if shared_net == 1:
            self.actor = nn.Sequential(nn.Linear(self.embedding_size, self.action_dim))
            self.critic = nn.Linear(self.embedding_size, 1)
            self.masker = nn.Linear(self.embedding_size, self.action_dim)
        else:
            self.actor = nn.Sequential(
                nn.Linear(self.embedding_size, hidden_size),
                fc_activation(),
                nn.Linear(hidden_size, hidden_size),
                fc_activation(),
                nn.Linear(hidden_size, self.action_dim),
            )
            self.critic = nn.Sequential(
                nn.Linear(self.embedding_size, hidden_size),
                fc_activation(),
                nn.Linear(hidden_size, hidden_size),
                fc_activation(),
                nn.Linear(hidden_size, 1),
            )
            self.masker = nn.Sequential(
                nn.Linear(self.embedding_size, hidden_size),
                fc_activation(),
                nn.Linear(hidden_size, hidden_size),
                fc_activation(),
                nn.Linear(hidden_size, self.action_dim),
            )

        # Contain data for training at each epoch
        self.buf = Buffer(
            grid_size,
            buf_size,
            action_dim=action_dim,
            gamma=self.discount,
            gae_lambda=self.gae_lambda,
        )

        # Set up optimizers for policy and value function
        self.optimizer = optimizer_func(self.parameters(), lr=learning_rate)
        self.train_iters = train_iters
        self.clip_ratio = clipping_ratio
        self.clip_norm = clip_norm
        self.entropy_ratio = entropy_ratio

    def _get_pi_and_v(self, image_info, mask, mask_inf):
        conv = self.conv(image_info)
        conv = conv.contiguous()
        embedding = conv.view(conv.shape[0], -1)

        if self.shared_net == 1:
            embedding = self.fc(embedding)

        logits = self.actor(embedding) * mask
        value = torch.squeeze(self.critic(embedding), -1)

        return Categorical(logits=logits + mask_inf), value

    def conv_out_size(self, net, h, w, c=None):
        """Helper function ot return the output size for a given input shape,
        without actually performing a forward pass through the model."""
        for child in net.children():
            try:
                h, w = conv2d_output_shape(
                    h, w, child.kernel_size, child.stride, child.padding
                )
            except AttributeError:
                pass  # Not a conv or maxpool layer.
            try:
                c = child.out_channels
            except AttributeError:
                pass  # Not a conv layer.
        return h * w * c

    def _get_pi_and_v_and_p_mask(self, grid_info, mask, mask_inf):
        conv = self.conv(grid_info)
        conv = conv.contiguous()
        embedding = conv.view(conv.shape[0], -1)

        if self.shared_net == 1:
            embedding = self.fc(embedding)

        logits = self.actor(embedding) * mask
        value = torch.squeeze(self.critic(embedding), -1)
        p_mask = self.masker(embedding)

        return Categorical(logits=logits + mask_inf), value, p_mask

    def _log_prob_from_distribution(self, pi, act):
        return pi.log_prob(act)

    def prob_from_distribution(self, pi, act):
        return pi.probs

    def step(self, obs, masks, eval=False, device=torch.device("cpu")):
        # case 2. height_map obs
        # before obs shape: (1, x_grid_size, y_grid_size)
        # image_info = torch.tensor(np.expand_dims(obs, axis=0), dtype=torch.float32).to(device)
        # image_info = torch.tensor(obs, dtype=torch.float32).to(device)
        # after obs shape: (1, 1, x_grid_size, y_grid_size)

        # case 3. height_map + mask obs
        # before obs shape: (3, x_grid_size, y_grid_size)
        grid_info = torch.tensor(np.expand_dims(obs, axis=0), dtype=torch.float32).to(
            device
        )
        # after obs shape: (1, 3, x_grid_size, y_grid_size)

        mask = torch.tensor(masks, dtype=torch.float32).to(device)
        mask_inf = torch.zeros_like(mask, dtype=torch.float32).to(device)
        mask_inf.masked_fill_(mask == 1, float(0.0))
        mask_inf.masked_fill_(mask == 0, float("-inf"))
        # mask_inf.masked_fill_(mask == 0, float(1e-3))

        with torch.no_grad():
            pi, v = self._get_pi_and_v(grid_info, mask, mask_inf)
            if not eval:
                a = pi.sample()
                logp_a = self._log_prob_from_distribution(pi, a)

                return (
                    a.detach().cpu().numpy(),
                    v.detach().cpu().numpy(),
                    logp_a.detach().cpu().numpy(),
                )
            else:
                a = torch.argmax(pi.probs)
                return a.detach().cpu().numpy()

    def update(self, device):
        # Obtain data from Buffer
        # torch.cuda.empty_cache()
        data = self.buf.get(device)

        obs, act, mask, adv, logp_old, ret, done = (
            data["obs"],
            data["act"],
            data["mask"],
            data["adv"],
            data["logp"],
            data["ret"],
            data["done"],
        )

        # image_info = torch.unsqueeze(obs, axis=0)
        # Train policy and value function with multiple steps of gradient descent
        loss_values = []
        loss_metrics = ["pi_loss", "value_loss", "entropy_loss", "mask_loss", "loss"]
        for _ in tqdm(range(self.train_iters)):
            self.optimizer.zero_grad()

            # Policy loss
            mask_inf = torch.zeros_like(mask, dtype=torch.float32).to(device)
            mask_inf.masked_fill_(mask == 1, float(0.0))
            # mask_inf.masked_fill_(mask == 0, float('-inf'))
            mask_inf.masked_fill_(mask == 0, float(1e-3))

            pi, v, p_mask = self._get_pi_and_v_and_p_mask(obs, mask, mask_inf)
            logp = self._log_prob_from_distribution(pi, act)

            ratio = torch.exp(logp - logp_old)
            clip_adv = (
                torch.clamp(ratio, 1 - self.clip_ratio, 1 + self.clip_ratio) * adv
            )
            pi_loss = -(torch.min(ratio * adv, clip_adv)).mean()

            # Value loss
            value_loss = self.value_loss_func(v, ret).mean()

            # Entropy loss
            entropy_loss = -torch.sum(
                pi.probs * torch.log(pi.probs + 1e-20), dim=-1
            ).mean()
            entropy_loss = self.entropy_ratio * entropy_loss

            # Feasible Mask loss
            mask_loss = 0.1 * self.mask_loss_func(p_mask, mask).mean()

            # Loss
            # loss function = pi_loss + value_loss - self.entropy_ratio * entropy_loss + 0.5 * mask_loss
            loss = pi_loss + value_loss - entropy_loss + mask_loss

            loss.backward()
            if self.clip_norm != 0:
                torch.nn.utils.clip_grad_norm_(self.parameters(), self.clip_norm)
            self.optimizer.step()

            loss_value = {
                "pi_loss": pi_loss.item(),
                "value_loss": value_loss.item(),
                "entropy_loss": entropy_loss.item(),
                "mask_loss": mask_loss.item(),
                "loss": loss.item(),
            }
            # loss_values.append(loss_value)
            loss_value = [
                pi_loss.item(),
                value_loss.item(),
                entropy_loss.item(),
                mask_loss.item(),
                loss.item(),
            ]
            loss_values.append(loss_value)

        loss_values = {
            metric: float(sum(loss) / len(loss))
            for loss, metric in zip(zip(*loss_values), loss_metrics)
        }
        return loss_values
